# Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
# file Copyright.txt or https://cmake.org/licensing for details.

#[=======================================================================[.rst:
FindHYDRA
-------

Find HYDRA instalation

- Original author: F.Uhlig@gsi.de (fairroot.gsi.de)
- Modifications: Rafal.Lalik@uj.edu.pl

Imported Targets
^^^^^^^^^^^^^^^^

This module provides the following imported targets, if found:

``hydra_all``
  All HYDRA 1 or 2 libraries

Result Variables
^^^^^^^^^^^^^^^^

This will define the following variables:

``HYDRA_FOUND``
  True if the system has the Hydra library.
``HYDRA_VERSION``
  The version of the Hydra library which was found.
``HYDRA_INCLUDE_DIRS``
  Include directories needed to use Hydra
``HYDRA_LIBRARIES``
  Libraries needed to link to Hydra
``HYDRA_GENERATION``
  Hydra generation, either AUTO, HYDRA or HYDRA2
``HYDRA_AVAILABLE_MODULES``
  List of modules

Cache Variables
^^^^^^^^^^^^^^^

The following cache variables may also be set:

``HYDRA_INCLUDE_DIR``
  The directory containing ``hades.h``.
``HYDRA_LIBRARY_DIR``
  The directory containing ``libHydra.so``.
``HYDRA_LIBRARY``
  The path to the Hydra library.

#]=======================================================================]

# Different set of modules for different Hydra generation
# cmake-format: off
set(HYDRA1_MODULES
    Alignment Dst HadesGo4 Hodo Hydra Hyp Kick MdcGarfield MdcPid Mdc MdcTrackD MdcTrackG MdcTrackS
    MdcUtil Pairs Particle PhyAna Pid PidUtil QA Revt Rich RichUtil Rpc Shower ShowerTofino
    ShowerUtil Simulation Start Tofino Tof TofUtil Tools Trigger TriggerUtil Wall Ora OraSim OraUtil
)

set(HYDRA2_MODULES
    Alignment Dst Emc EventDisplay Forward FRpc Hydra iTof Kalman MdcGarfield Mdc MdcTrackD:Mdc
    MdcTrackG MdcUtil Online Particle:Emc,FRpc,iTof,Kalman,MdcTrackD,MdcTrackG PionTracker QA Revt
    Rich Rpc Shower ShowerUtil Simulation Start Sts Tof Tools Wall Ora OraSim OraUtil
)
# cmake-format: on

set(HYDRA_GENERATION
    AUTO
    CACHE STRING "Hydra software generation")
set_property(CACHE HYDRA_GENERATION PROPERTY STRINGS AUTO;HYDRA2;HYDRA)

if(NOT HADDIR AND DEFINED ENV{HADDIR})
  set(HADDIR $ENV{HADDIR})
else()
  set(HADDIR)
endif()

if(NOT HADDIR)
  message(STATUS "HADDIR is not set. Please set HADDIR first.")
endif()

if(NOT MYHADDIR AND DEFINED ENV{MYHADDIR})
  set(MYHADDIR $ENV{MYHADDIR})
endif()

message(STATUS "Looking for Hydra: generation = ${HYDRA_GENERATION}...")
message(STATUS "                       HADDIR = ${HADDIR}")
if(MYHADDIR)
message(STATUS "                     MYHADDIR = ${MYHADDIR}")
endif()

set(HYDRA_PACKAGE_SEARCHPATH ${HADDIR}/lib)

set(HYDRA_DEFINITIONS "")

set(HYDRA_INSTALLED_VERSION_TOO_OLD FALSE)

find_library(
  HYDRA_MAIN
  NAMES Hydra
  PATHS ${HYDRA_PACKAGE_SEARCHPATH}
  NO_DEFAULT_PATH)

if(HYDRA_MAIN_FOUND)
  message(STATUS "Hydra library not found. Please check your Hydra installation.")
else()

  set(HYDRA_LIBRARY_DIR ${HADDIR}/lib)
  set(HYDRA_INCLUDE_DIR ${HADDIR}/include)
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(HYDRA DEFAULT_MSG HADDIR HYDRA_MAIN HYDRA_LIBRARY_DIR HYDRA_INCLUDE_DIR)

unset(HYDRA_MAIN CACHE)

if(HYDRA_FOUND)
  # if option AUTO search for libPid library which is available
  # in Hydra1, if not found mark as Hydra2
  if(HYDRA_GENERATION STREQUAL "AUTO")
    find_library(
      HYDRA_HYDRA1_TEST_LIBRARY
      NAMES Pid
      PATHS ${HYDRA_PACKAGE_SEARCHPATH}
      NO_DEFAULT_PATH)

      if (HYDRA_HYDRA1_TEST_LIBRARY_FOUND)
        set(HYDRA_GENERATION "HYDRA")
      else()
        set(HYDRA_GENERATION "HYDRA2")
      endif()

      unset(HYDRA_HYDRA1_TEST_LIBRARY CACHE)
  endif()

  if(HYDRA_GENERATION STREQUAL "HYDRA2")
    set(HYDRA_AVAILABLE_MODULES ${HYDRA2_MODULES})
  elseif(HYDRA_GENERATION STREQUAL "HYDRA")
    set(HYDRA_AVAILABLE_MODULES ${HYDRA1_MODULES})
  endif()

  add_library(hydra_all INTERFACE IMPORTED)
  add_library(HYDRA::all ALIAS hydra_all)

  set(HYDRA_LIBRARIES)

  foreach(_cpt_string ${HYDRA_AVAILABLE_MODULES})
    string(REPLACE ":" ";" _cpt_list ${_cpt_string})

    list(GET _cpt_list 0 _cpt)
    list(LENGTH _cpt_list _cpt_list_length)

    find_library(
      HYDRA_${_cpt}_LIBRARY
      NAMES ${_cpt} lib{_cpt}
      HINTS ${HYDRA_LIBRARY_DIR})

    if(HYDRA_${_cpt}_LIBRARY)
      mark_as_advanced(HYDRA_${_cpt}_LIBRARY)
      list(APPEND HYDRA_LIBRARIES ${HYDRA_${_cpt}_LIBRARY})
      add_library(${_cpt} SHARED IMPORTED)
      target_include_directories(${_cpt} INTERFACE ${HYDRA_INCLUDE_DIR})
      target_link_directories(${_cpt} INTERFACE ${HYDRA_LIBRARY_DIR})
      set_target_properties(
        ${_cpt} PROPERTIES IMPORTED_CONFIGURATIONS RELEASE
        IMPORTED_LOCATION_RELEASE "${HYDRA_${_cpt}_LIBRARY}"
                   IMPORTED_SONAME_RELEASE "lib${_cpt}.so")
      add_library(HYDRA::${_cpt} ALIAS ${_cpt})

      target_link_libraries(hydra_all INTERFACE HYDRA::${_cpt})

      if (_cpt_list_length GREATER 1)
        list(GET _cpt_list 1 _cpt_deps_string)
        string(REPLACE "," ";" _cpt_deps_list ${_cpt_deps_string})

        foreach(_cpt_dep ${_cpt_deps_list})
          target_link_libraries(${_cpt} INTERFACE ${_cpt_dep})
        endforeach()
      endif()

    else()
      if(HYDRA_FIND_REQUIRED_${_cpt})
        message(FATAL_ERROR "Hydra component ${_cpt} not found")
      elseif(${_cpt} IN_LIST HYDRA_FIND_COMPONENTS AND NOT HYDRA_FIND_QUIETLY)
        message(WARNING " Hydra component ${_cpt} not found")
      endif()
    endif()
  endforeach()
  if(HYDRA_LIBRARIES)
    list(REMOVE_DUPLICATES HYDRA_LIBRARIES)
  endif()

  # Make variables changeble to the advanced user
  mark_as_advanced(HYDRA_LIBRARY_DIR HYDRA_INCLUDE_DIR HYDRA_DEFINITIONS)

  # Set HYDRA_INCLUDES
  set(HYDRA_INCLUDES ${HYDRA_INCLUDE_DIR})
endif()

